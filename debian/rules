#!/usr/bin/make -f
export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
include /usr/share/dpkg/default.mk

# If SALSABUILD is set we want to drop debug info as well
ifneq ($(SALSABUILD),)
  export DEB_CXXFLAGS_MAINT_APPEND += -g0
endif

# no debug info to avoid running
# out of address space when linking
ifneq (,$(filter $(DEB_HOST_ARCH), mipsel))
	export DEB_CXXFLAGS_MAINT_APPEND += -g0
endif

SHARED_LIB_PATH = shared_lib
STATIC_LIB_PATH = static_lib

# compute current and next Eigen version and add it to d/control
# Needed because CeresConfig.cmake sets it in CERES_EIGEN_VERSION
# also see #868355
eigenvercurr := $(shell dpkg-query -W -f '$${Source:Upstream-Version}' libeigen3-dev | awk -F. '{print $$1"."$$2"."$$3}')
eigenvernext := $(shell dpkg-query -W -f '$${Source:Upstream-Version}' libeigen3-dev | awk -F. '{print $$1"."$$2"."$$3+1}')

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -B$(SHARED_LIB_PATH) -- \
	        -DBUILD_SHARED_LIBS=ON \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_TESTING=ON \
		-DSUITESPARSE_LIBRARY_DIR_HINTS=/usr/lib/${DEB_HOST_MULTIARCH}/ \
		-DSUITESPARSE_INCLUDE_DIR_HINTS=/usr/include/suitesparse/ \
		-DCXSPARSE_LIBRARY_DIR_HINTS=/usr/lib/${DEB_HOST_MULTIARCH}/ \
		-DCXSPARSE_INCLUDE_DIR=/usr/include/suitesparse/ \
		-DBUILD_DOCUMENTATION=$(if $(filter ceres-solver-doc,$(shell dh_listpackages)),ON,OFF)
	dh_auto_configure -B$(STATIC_LIB_PATH) -- \
	        -DBUILD_SHARED_LIBS=OFF \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_TESTING=OFF \
		-DSUITESPARSE_LIBRARY_DIR_HINTS=/usr/lib/${DEB_HOST_MULTIARCH}/ \
		-DSUITESPARSE_INCLUDE_DIR_HINTS=/usr/include/suitesparse/ \
		-DCXSPARSE_LIBRARY_DIR_HINTS=/usr/lib/${DEB_HOST_MULTIARCH}/ \
		-DCXSPARSE_INCLUDE_DIR=/usr/include/suitesparse/ \
		-DBUILD_DOCUMENTATION=OFF \
		-DCMAKE_BUILD_TYPE=Release

override_dh_auto_build:
	dh_auto_build -B$(SHARED_LIB_PATH)
	dh_auto_build -B$(STATIC_LIB_PATH)

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test -B$(SHARED_LIB_PATH)
endif

override_dh_auto_clean:
	dh_auto_clean
	-rm -rf $(SHARED_LIB_PATH) $(STATIC_LIB_PATH)

execute_before_dh_gencontrol:
	echo 'eigen:Upstream-Version=$(eigenvercurr)' >> debian/libceres-dev.substvars
	echo 'eigen:Upstream-Version-Next=$(eigenvernext)' >> debian/libceres-dev.substvars

override_dh_compress:
	dh_compress -X.cc -X.h

override_dh_auto_install:
	# Installing the shared lib _after_ the static one is very important so that
	# the remaining CeresTargets (and friends) files in /usr/lib/*/cmake/Ceres are
	# allowing one to link with the shared lib of Ceres, not the static one.
	dh_auto_install -B$(STATIC_LIB_PATH)
	dh_auto_install -B$(SHARED_LIB_PATH)

override_dh_installdocs-indep:
	# make lintian happy
	# https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0 -> /usr/share/javascript/mathjax
	sed -i 's/https:\/\/cdnjs.cloudflare.com\/ajax\/libs\/mathjax\/2.7.1/\/usr\/share\/javascript\/mathjax/g' $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/*.html
	sed -i 's/https:\/\/cdn.jsdelivr.net//g' $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/*.html
	rm $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/doctools.js
	ln -s /usr/share/javascript/sphinxdoc/1.0/doctools.js $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/doctools.js
	rm -f $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/js/html5shiv-printshiv.min.js
	ln -s /usr/share/javascript/html5shiv/html5shiv-printshiv.min.js $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/js/html5shiv-printshiv.min.js
	rm -f $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/js/html5shiv.min.js
	ln -s /usr/share/javascript/html5shiv/html5shiv.min.js $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/js/html5shiv.min.js
	rm -f $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/language_data.js
	ln -s /usr/share/javascript/sphinxdoc/1.0/language_data.js $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/language_data.js
	rm -f $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/searchtools.js
	ln -s /usr/share/javascript/sphinxdoc/1.0/searchtools.js $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/searchtools.js
	rm -f $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/jquery.js
	ln -s /usr/share/javascript/jquery/jquery.js $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/jquery.js
	rm -f $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/underscore.js
	ln -s /usr/share/javascript/underscore/underscore.js $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/underscore.js
	rm -rf $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/fonts
	ln -s /usr/share/fonts/truetype/lato $(CURDIR)/debian/tmp/usr/share/doc/Ceres/html/_static/fonts
	dh_installdocs
